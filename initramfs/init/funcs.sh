set -euo pipefail

has_cmdline_param() {
	local got=""
	for o in $(cat /proc/cmdline); do
	case "$o" in
		$1) got="yes"; ;;
	esac
	done
	echo "$got"
}

setup_dirs() {
	mkdir -p /proc /sys /dev /mnt
	mount -t proc proc /proc
	mount -t sysfs sysfs /sys
	mount -t devtmpfs devtmpfs /dev
}

cleanup_dirs() {
	umount /proc /sys /mnt || true
}

# inherits /dev into mounted (aka real) rootfs.
inherit_dev() {
	mkdir -p /rootfs/dev 
	mount --move /dev /rootfs/dev || true
}

wait_file() {
	local filename="$1"
	local waitsecs="$2"
	while [ ! -e "$filename" ]; do
		if [ $waitsecs -eq "0" ]; then
			return 1
		fi
		echo "waiting for ${filename}: remaining: ${waitsecs}s"
		waitsecs=$((waitsecs-1))
		sleep 1
	done
	return 0
}

set_consolefont() {
	test -e "$1" || {
		echo "font file $1 not found: skipping setting console font"
		return 0
	}
	wait_file "/dev/console" 2 || {
		echo "/dev/console not found yet: skipping setting console font"
		return 0
	}
	setfont -C /dev/console "$1"
}

# runs a niladic function ($2)
# n ($1) times until success.
try_n () {
	local n=$1
	local i=0
	while [ $i -lt $n ]; do
		$2 && break
		i=$((i+1))
	done
}

# unlock all disks using cryptsetup.
unlock_all() {
	# get shared password
	read -s -p "disks: " pw # reportedly not posix, but works in busybox read
	echo                    # move to next line after password input
	echo "..."              # show some indication of progress

	# cryptsetup v2.x requires /run for locking.
	mkdir -p /run
	
	# do the unlocking 
	local pids=""
	local opts=" --verbose --type luks2 --perf-no_read_workqueue --perf-no_write_workqueue "
	printf "$pw" | cryptsetup open $opts $swap $mappedswap & pids="${pids} $!"
	printf "$pw" | cryptsetup open $opts $root $mappedroot & pids="${pids} $!"
	
	pw=""           # clear it fwiw
	wait_pids $pids # must not quote $pids, must be passed as multiple separate args.
}

# returns 0 if all succeed, 1 otherwise.
# if a process fails, the other processes may not be waited upon
# (but may have already finished or finish later).
wait_pids() {
	local retval=0
	for p; do
		if [ $retval -eq "0" ]; then
			wait $p || retval=1
		fi
	done
	return $retval
}
