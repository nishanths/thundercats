#!/bin/sh

set -euo pipefail

# usage: $0 [path/to/out.cpio]
# if the output path is omitted, it defaults to "/boot/initramfs.cpio"

dst="/boot/initramfs.cpio"
[ $# -gt 0 ] && dst=`realpath "$1"`
vendordir=`realpath vendor` workdir=$(mktemp -d)

echo "work directory: $workdir" # to allow for inspection if something goes awry

mkdir "${workdir}"/bin "${workdir}"/dev

# the init executable
cp init/* "$workdir"

# other binaries and executables.
# the binaries are assumed to be static.
# i.e. ldd will report no dynamic library dependencies.
cp "${vendordir}"/busybox "${workdir}"/bin/busybox
( cd "${workdir}"/bin && ln -s busybox sh )
cp "${vendordir}"/cryptsetup "${workdir}"/bin/cryptsetup
cp "${vendordir}"/fsck "${workdir}"/bin/fsck

# essential dev files.
# see kernel's Documentation/devices.txt
# for reference.
mknod -m 622 "${workdir}"/dev/null c 1 3
mknod -m 622 "${workdir}"/dev/random c 1 8  # needed for cryptsetup?
mknod -m 622 "${workdir}"/dev/urandom c 1 9 # "
mknod -m 622 "${workdir}"/dev/tty0 c 4 0
mknod -m 622 "${workdir}"/dev/tty c 5 0
mknod -m 622 "${workdir}"/dev/console c 5 1
mknod -m 622 "${workdir}"/dev/loop0 b 7 0

cp "${vendordir}"/consolefont "${workdir}"/consolefont

# write the cpio
# note: "$dst" must be an absolute path to work as expected in cd'd subshell
( cd "$workdir" && find . | cpio -H newc -o > "$dst" && echo "wrote output: $dst" )
