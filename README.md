This repository has utilities for working with Sabotage Linux.
Some of these may be specific to how I set up my computer, but
I've tried to make a note of this (e.g. "For my use, ...")
in such spots.

# CLONE

Clone/copy this repository.

	mkdir -p /local/src
	git clone <url> /local/src/sabotage-linux-tools

# EXTRA PACKAGES

Put custom packages in KEEP/ and pkg/.

The contents of KEEP/ and pkg/ should be added to /src/KEEP/
and /src/pkg/ respectively to be usable with butch.
The script copypkgs.sh can do this.

If package names would conflict with official sabotage package
names, suffix an 'x' to the name. For example, kernel510x because
kernel510 is an official sabotage package.

# INSTALLING SABOTAGE LINUX

Follow the Sabotage Linux README and COOKBOOK.
For my use, after stage1 is installed installing the "core"
meta package isn't necessary.

# KERNEL

To install the kernel, use the kernelx packages
in this repo. For example:

	butch install kernel510x

Note: You may not have the necessary
host dependencies to use xz compression for the kernel
image (busybox's xz does not support some options
used by the kernel build). gzip compression for
the boot image shoud work.

The kernelx packages have a goal of keeping patching to a minimum.
Sometimes, not patching requires taking on an additional host dependency.
Taking on a dependency such as perl (kernel510x does this),
instead of patching in awk replacement code to me is a
good tradeoff (perl is required for many other packages anyway). But for a
heavier dependency, patching will be better.

There is a kernel config file in KEEP/ that should work with the latest kernelx
package. The config is intended for a
Thinkpad X1 Carbon Gen 7 with a non-vPro Core i5-8265U.
The kernel config was originally designed by merging
a default gentoo kernel config and a default sabotage kernel config.

Kernel firmware such as microcode (suitable for my hardware) and wireless regdb
files are present under KEEP/. The kernel config uses the right directory.

# DISKS

For disk partitioning and disk encryption during installation to
a hard disk, use the prepare-disk.sh script. It sets up the following
partitions.

Each encrypted partition will have two keyslots: (0) "typeable password",
and (1) "long key". All encrypted paritions share the same typeable
password, but they all have different long keys.

	#:  device		mount	fs	  size	encryption
	1  /dev/nvme0n1p1	/boot	vfat	  256M	-
	2  /dev/nvme0n1p2	[SWAP]	swap	16384M	luks2
	3  /dev/nvme0n1p3	/	ext4	remain.	luks2

Note: the sabotage cryptsetup package at the time of writing provides
cryptsetup v1, which doesn't support luks2. So this repository
includes a cryptsetup2 package that provides cryptsetup v2.

# INITRAMFS

See the initramfs/ directory. It contains utilities for making an initramfs
cpio archive, which can be specified as the "initrd" kernel command line value.
The initramfs is necessary to decrypt the rootfs.

Directory structure:

	installcpio.sh  script that creates initramfs cpio archive
	init/           contains initramfs init script
	vendor/         resources used by init script, vendored
	                to avoid unintentional changes.

Copy necessary binaries (cryptsetup, busybox) into the vendor
directory before running the script. Add your favorite console font
to a file named "consolefont" in vendor/ and it will be used in
the initramfs.

The init script is hardcoded for my use (disk device names, disk
encryption unlocking), but changes are welcome.

# BOOT MANAGER

See boot-manager.sh.
The default kernel image output location and the default initramfs output
location will work with the script as is.

# SYSTEM CONFIGURATION

After the system is ready, add these vars to /src/config. I prefer ncurses now.

	export OPT_SPEED=1
	export DEPS=all:curses.ncurses

Most of the other changes will be in rootfs's /etc.

# USEFUL PACKAGES

	git
	openssh
	pwgen
	tree
	file
	su
	wireless-tools
	wpa-supplicant
	curl
	wget
	xz
	posix-man
	cryptsetup2
	vim
	silversearcher

TODO: silversearcher seems like a really old version.

# TODO

* wpa\_supplicant
* fsck in initramfs
* install efibootmgr (requires linux headers)
