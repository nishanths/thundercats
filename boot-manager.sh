#!/bin/sh

set -euo pipefail

# usage: $0
# creates an EFI boot entry.
# the program accepts no arguments.
#
# only designed for GPT and UEFI systems.
# it expects that the efibootmgr package is installed.
# the system must be able to access efi vars. this requires kernel support.
# see the kernel.config in the sabotagetools repo for a (likely overzealous)
# sample.

bootdev=/dev/nvme0n1p1
p=1

# copy kernel to the right location.
cp /boot/vmlinuz /boot/efi/bootx64.efi

# delete existing efi entry if any.
>/dev/null 2>&1 efibootmgr -B -b1 || true

# create new efi entry.
>/dev/null efibootmgr -c -b1 -d "$bootdev" -p "$p" -L Linux \
	-l '\efi\boot\bootx64.efi' \
	-u 'initrd=\initramfs.cpio quiet i915.fastboot=1'
